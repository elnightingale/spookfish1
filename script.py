import cv2
import numpy as np
import math

img = cv2.imread('image1.png')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(gray, 75, 150)
lines = cv2.HoughLinesP(edges, 1, np.pi/180, 100, maxLineGap=20, minLineLength=100)

#calculate the line lengths
lineLengths = []
print('there are ' + str(len(lines)) + ' lines')
for line in lines:
    x1, y1, x2, y2 = line[0]
    lineLength = math.hypot(x2 - x1, y2 - y1)
    lineLengths.append((line[0], lineLength))

#sort the lines by their length
lineLengths = sorted(lineLengths, key=lambda x: x[1], reverse=True)
amountOfLines =  7
i = 0

#only paint the highest lines on the picture
for line in lineLengths:
    if (i > amountOfLines):
        break
    x1, y1, x2, y2 = line[0]
    cv2.line(img, (x1, y1), (x2, y2), (0, 255, 0), 3)
    i = i + 1 
 
#cv2.imwrite('savedImage.png', img) -> save the image

cv2.imshow("Edges",edges)
cv2.imshow("Image", img)
cv2.waitKey(0)
cv2.destroyAllWindows()


#lines = cv2.HoughLines(edges,1,np.pi/180,200)
#for rho,theta in lines[0]:
#    a = np.cos(theta)
 #   b = np.sin(theta)
  #  x0 = a*rho
  #  y0 = b*rho
  #  x1 = int(x0 + 1000*(-b))
  #  y1 = int(y0 + 1000*(a))
  #  x2 = int(x0 - 1000*(-b))
  #  y2 = int(y0 - 1000*(a))

   # cv2.line(img,(x1,y1),(x2,y2),(0,0,255),2)

#cv2.imwrite('houghlines3.jpg',img)